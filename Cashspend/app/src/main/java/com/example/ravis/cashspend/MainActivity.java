
package com.example.ravis.cashspend;


import android.support.v7.app.ActionBar;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.Toolbar;
        import android.app.DatePickerDialog;
        import android.icu.text.SimpleDateFormat;
        import android.icu.util.Calendar;
        import android.support.v7.app.AppCompatActivity;
        import android.os.Bundle;
        import android.view.View;
        import android.widget.AdapterView;
        import android.widget.ArrayAdapter;
        import android.widget.CompoundButton;
        import android.widget.DatePicker;
        import android.widget.EditText;
        import android.widget.Spinner;
        import android.widget.Switch;
        import android.widget.Toast;

        import java.util.ArrayList;
        import java.util.List;
        import java.util.Locale;

public class MainActivity extends AppCompatActivity {
Toolbar addcashspendtoolbar;
    EditText datepicker;
    Switch moneyspend;
    Spinner selectcategory;
    //Calendar c = Calendar.getInstance();
    int cday, cmonth, cyear;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        addcashspendtoolbar=(Toolbar)findViewById(R.id.addcashspendtollbar);
       setSupportActionBar(addcashspendtoolbar);
        ActionBar ab=getSupportActionBar();
        ab.setDisplayHomeAsUpEnabled(true);



        datepicker=(EditText)findViewById(R.id.datepicker) ;
       /* datepicker.setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View arg0) {
                // TODO Auto-generated method stub
                new DatePickerDialog(MainActivity.this, d,
                        c.get(Calendar.YEAR), c.get(Calendar.MONTH), c
                        .get(Calendar.DAY_OF_MONTH)).show();

            }
        });
*/
        moneyspend=(Switch)findViewById(R.id.moneyspendchoice);
        moneyspend.setChecked(true);
        moneyspend.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {

            @Override
            public void onCheckedChanged(CompoundButton buttonView,
                                         boolean isChecked) {

                if(isChecked){
                    moneyspend.setText("Spend");
                }else{
                    moneyspend.setText("Not Spend");
                }

            }
        });


        selectcategory=(Spinner)findViewById(R.id.selectcategory);
       selectcategory.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
           @Override
           public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {

               String item = parent.getItemAtPosition(position).toString();
               Toast.makeText(parent.getContext(), "Selected: " + item, Toast.LENGTH_LONG).show();
           }

           @Override
           public void onNothingSelected(AdapterView<?> parent) {

           }
       });

        List<String> categories = new ArrayList<String>();
        categories.add("Automobile");
        categories.add("Business Services");
        categories.add("Computers");
        categories.add("Education");
        categories.add("Personal");
        categories.add("Travel");
        ArrayAdapter<String> dataAdapter = new ArrayAdapter<String>(this, android.R.layout.simple_spinner_item, categories);
        dataAdapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        selectcategory.setAdapter(dataAdapter);


    }

    DatePickerDialog.OnDateSetListener d = new DatePickerDialog.OnDateSetListener() {

        @Override
        public void onDateSet(DatePicker view, int year, int monthOfYear,
                              int dayOfMonth) {

            cday = dayOfMonth;
            cmonth = monthOfYear + 1;
            cyear = year;

            datepicker.setText( cday + "/" + cmonth + "/"
                    + cyear);
        }
    };

}

